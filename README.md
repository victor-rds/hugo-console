# Hugo Theme: Console

A minimal, responsive and light theme for Hugo inspired by Linux console. 

![Console](https://gitlab.com/victor-rds/hugo-console/blob/master/images/preview.png?raw=true)

## Live demo

https://mrmierzejewski.com

## Installation

```sh
$ mkdir themes
$ cd themes
$ git submodule add https://gitlab.com/victor-rds/hugo-console.git hugo-console
```
    
See the [Hugo documentation](https://gohugo.io/themes/installing/) for more information.

## Configuration

Set theme parameter in your config file:

```
theme = "hugo-console"
```

## Example Site

To run the example site, please type the following command:

```
makefile hugo-server
```

### Start page

The default start page template is located at ```themes/hugo-console/layouts/index.html```. To change the page contect, you to need to copy this file to 
your website top-level ```layouts``` folder (```layouts/index.html```).

## License

Copyright © 2020 [Marcin Mierzejewski](https://mrmierzejewski.com/)

The theme is released under the MIT License. Check the [original theme license](https://github.com/panr/hugo-theme-terminal/blob/master/LICENSE.md) for additional licensing information.
