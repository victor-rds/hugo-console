+++
date = "2016-11-05T21:05:33+05:30"
title = "About"
+++

A minimal, responsive and light theme for Hugo inspired by Linux console. 

![Console](https://gitlab.com/victor-rds/hugo-console/blob/master/images/preview.png?raw=true)

## Installation

```
$ mkdir themes
$ cd themes
$ git submodule add https://gitlab.com/victor-rds/hugo-console.git hugo-console
```
    
See the [Hugo documentation](https://gohugo.io/themes/installing/) for more information.

## Configuration

Set theme parameter in your config file:

```
theme = "hugo-console"
```

## License

Copyright © 2020 [Marcin Mierzejewski](https://mrmierzejewski.com/)

The theme is released under the MIT License. Check the [original theme license](https://github.com/panr/hugo-theme-terminal/blob/master/LICENSE.md) for additional licensing information.
